#!/bin/bash
## Author: Michael Ramsey
## Enable HTTP_AUTHORIZATION in Apache vhost for a cPanel user. 
## nano enable_http_auth.sh
## run as root sh enable_http_auth.sh username

#Ensure "userdata/ssl/2_4/" data structure is created Source:https://forums.cpanel.net/threads/add-line-of-code-to-vhost.598267/
/scripts/ensure_vhost_includes --all-users


#Find Apache vHost directory structure to add rule
if [ -d "/etc/apache2/conf.d/userdata/ssl/2_4/" ]; then
   #echo "Folder /usr/local/apache/conf/userdata/ssl/2_4/ exists"
   CurrentApacheVhostPathSSL='/etc/apache2/conf.d/userdata/ssl/2_4/'
   CurrentApacheVhostPathSTD='/etc/apache2/conf.d/userdata/std/2_4/'
   echo "$CurrentApacheVhostPathSSL"
elif [ -d "/usr/local/apache/conf/userdata/ssl/2_4/" ]; then
   #echo "Folder /usr/local/apache/conf/userdata/ssl/2_4/ exists"
   CurrentApacheVhostPathSSL='/usr/local/apache/conf/userdata/ssl/2_4/'
   CurrentApacheVhostPathSTD='/usr/local/apache/conf/userdata/std/2_4/'
   echo "$CurrentApacheVhostPathSSL"
elif [ -d "/usr/local/apache/conf/userdata/ssl/2_2/" ]; then
   #echo "Folder /usr/local/apache/conf/userdata/ssl/2_2/ exists"
   CurrentApacheVhostPathSSL='/usr/local/apache/conf/userdata/ssl/2_2/'
   CurrentApacheVhostPathSTD='/usr/local/apache/conf/userdata/std/2_2/'
   echo "$CurrentApacheVhostPathSSL"
elif [ -d "/usr/local/apache/conf/userdata/ssl/2/" ]; then
   #echo "Folder /usr/local/apache/conf/userdata/ssl/2/ exists"
   CurrentApacheVhostPathSSL='/usr/local/apache/conf/userdata/ssl/2/'
   CurrentApacheVhostPathSTD='/usr/local/apache/conf/userdata/std/2/'
   echo "$CurrentApacheVhostPathSSL"
else
   echo "Unable to detect current CurrentApacheVhostPath for this server"
   echo "Creating folder structure"
   CurrentApacheVhostPathSSL='/etc/apache2/conf.d/userdata/ssl/2_4/'
   CurrentApacheVhostPathSTD='/etc/apache2/conf.d/userdata/std/2_4/'
   echo "$CurrentApacheVhostPathSSL"
   mkdir -p "$CurrentApacheVhostPathSSL"
   mkdir -p "$CurrentApacheVhostPathSTD"
fi


#Make needed path and file with proper permissions for SSL https
mkdir -p "$CurrentApacheVhostPathSSL""$1"/
chmod 0755 "$CurrentApacheVhostPathSSL""$1"/
touch "$CurrentApacheVhostPathSSL""$1"/auth.conf
chmod 0644 "$CurrentApacheVhostPathSSL""$1"/auth.conf
echo 'SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1' >> "$CurrentApacheVhostPathSSL""$1"/auth.conf
echo "$CurrentApacheVhostPathSSL$1/auth.conf created"
cat "$CurrentApacheVhostPathSSL""$1"/auth.conf

#Make needed path and file with proper permissions for STD http
mkdir -p "$CurrentApacheVhostPathSTD""$1"/
chmod 0755 "$CurrentApacheVhostPathSTD""$1"/
touch "$CurrentApacheVhostPathSTD""$1"/auth.conf
chmod 0644 "$CurrentApacheVhostPathSTD""$1"/auth.conf
echo 'SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1' >> "$CurrentApacheVhostPathSTD""$1"/auth.conf
echo "$CurrentApacheVhostPathSTD$1/auth.conf created"
cat "$CurrentApacheVhostPathSTD""$1"/auth.conf

/scripts/rebuildhttpdconf 

echo 'If the rebuildhttpdconf completed without errors please do restart httpd services via'
echo 'service httpd restart'